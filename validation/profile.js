const Validator = require('validator');
const isEmpty = require('./is-empty');

module.exports = function validateProfileInput(data) {
  let errors = {};

  data.handle = !isEmpty(data.handle) ? data.handle : '';
  data.poste = !isEmpty(data.poste) ? data.poste : '';
  

  if (!Validator.isLength(data.handle, { min: 2, max: 40 })) {
    errors.handle = 'La poignée doit contenir entre 2 et 40 caractères';
  }

  if (Validator.isEmpty(data.handle)) {
    errors.handle = 'La poignée profilée est requise';
  }

  if (Validator.isEmpty(data.poste)) {
    errors.poste = 'le champ poste est obligatoire';
  }

  

  if (!isEmpty(data.emailadr)) {
    if (!Validator.isURL(data.emailadr)) {
      errors.emailadr = 'Pas une URL valide';
    }
  }



  if (!isEmpty(data.facebook)) {
    if (!Validator.isURL(data.facebook)) {
      errors.facebook = 'Pas une URL valide';
    }
  }

  if (!isEmpty(data.linkedin)) {
    if (!Validator.isURL(data.linkedin)) {
      errors.linkedin = 'Pas une URL valide';
    }
  }

  if (!isEmpty(data.instagram)) {
    if (!Validator.isURL(data.instagram)) {
      errors.instagram = 'Pas une URL valide';
    }
  }

  return {
    errors,
    isValid: isEmpty(errors)
  };
};
